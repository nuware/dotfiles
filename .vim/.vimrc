set nocompatible
set clipboard+=unnamed

" Init Vundle
filetype off
set rtp+=$GOROOT/misc/vim/
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
" Let Vundle manage Vundle
Bundle 'gmarik/vundle'

Bundle 'tpope/vim-rails'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-commentary'
Bundle 'tpope/vim-markdown'
Bundle 'flazz/vim-colorschemes'
Bundle 'scrooloose/syntastic'
Bundle 'vim-scripts/ctags.vim'
Bundle 'othree/html5.vim'

filetype on
filetype plugin on
filetype indent on

autocmd FileType go autocmd BufWritePre <buffer> Fmt

let mapleader=","

let g:html_indent_inctags = "html,body,head,tbody"

" syntastic
let g:syntastic_check_on_open=1

" html5.vim
let g:html5_event_handler_attributes_complete = 0
let g:html5_rdfa_attributes_complete = 0
let g:html5_microdata_attributes_complete = 0
let g:html5_aria_attributes_complete = 0

set t_Co=256
set term=xterm-256color

set background=dark
colorscheme jellybeans

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if (&t_Co > 2 || has("gui_running")) && !exists("syntax_on")
  syntax on
  set guifont=PT\ Mono:h11
endif

" Mouse
set ttyfast
set ttymouse=xterm2
if has("mouse")
  set mousehide
  set mouse=a
endif

" UI
set encoding=utf-8
set nowrap
set showcmd
set showmode
set cursorline
set showtabline=2
set number
set numberwidth=5
set ruler
set cmdheight=2
set laststatus=2
set noerrorbells
set novisualbell
set nobackup
set noswapfile

" Text
set autoindent
set smartindent
set linebreak
set tabstop=2
set shiftwidth=2
set expandtab
set scrolloff=5
set textwidth=80

set backspace=indent,eol,start
"set list listchars=tab:··,trail:·
set list listchars=tab:»·,trail:·

" Search
set hlsearch
set incsearch
set ignorecase
set smartcase

" Behaviors
set wildmenu
set foldenable
set foldmethod=syntax
set foldlevel=1
set foldlevelstart=20

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" Quicker window movement
"nnoremap <C-j> <C-w>j
"nnoremap <C-k> <C-w>k
"nnoremap <C-h> <C-w>h
"nnoremap <C-l> <C-w>l

" Treat <li> and <p> tags like the block tags they are
let g:html_indent_tags = 'li\|p'

" Snippets are activated by Shift+Tab
"let g:snippetsEmu_key = "<S-Tab>"

" Exclude Javascript files in :Rtags via rails.vim due to warnings when parsing
"let g:Tlist_Ctags_Cmd="ctags --exclude='*.js'"

" Index ctags from any project, including those outside Rails
map <Leader>ct :!ctags -R .<CR>

