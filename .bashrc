#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


[[ -s "$HOME/.local/ps1_functions" ]] && source "$HOME/.local/ps1_functions"

alias ls='ls -G'
alias l='ls -lh'

ps1_set --prompt §
