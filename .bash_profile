#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && source ~/.bashrc

export EDITOR=vim
#export VISUAL=vim xterm
#export TERM=xterm

# RubyMine
export PATH=~/RubyMine-4.5.4/bin:$PATH

# Ruby
export GEM_HOME=~/.gem/ruby/1.9.1
export PATH=~/.gem/ruby/1.9.1/bin:$PATH

export PATH=~/bin:$PATH

# Browser
if [ -n "$DISPLAY" ]; then
  export BROWSER=chromium
else
  export BROWSER=links
fi